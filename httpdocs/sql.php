<?php
  /**************************************************************************************************/
  /*                                                                                                */
  /*                                 FICHERO DE EJERCICIO SQL                                       */
  /*                                                                                                */
  /**************************************************************************************************/

    $path_raiz = './';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  $tituloPagina = 'SQL';
  $menuSel = 'sql';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

?>


<!DOCTYPE html>
<html>

    <?php include_once 'docs/includes/head.php'; ?>

    <body class="hold-transition sidebar-mini layout-fixed">
      <div class="wrapper">

        <?php include_once 'docs/includes/sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

          <?php include_once 'docs/includes/header.php'; ?>

          <!-- Main content -->
          <section class="content">

            <div class="container-fluid">

              <div class="callout callout-info">
                <h5><i class="fas fa-info"></i> SQL</h5>
                La base de datos necesaria está en el directorio raíz, con el nombre database.sql
                Realiza los siguientes SQL's y copia en la sección resultados la consulta final (la imagen es sólo como ayuda, el resultado no tiene por qué coincidir).
                <ol>
                  <li>Media de los anticipos cobrados de los alojamientos activos.</li>
                  <li>Número total de habitaciones de cada hotel (sólo activos), mostrando Nombre de hotel – total de habitaciones.
                    <div class="col-md-12 text-center"><img src="dist/img/sql2.png" /></div>
                  </li>
                  <li>Propietarios registrados en septiembre.</li>
                  <li>Número total de propietarios registrados cada mes.
                    <div class="col-md-12 text-center"><img src="dist/img/sql4.png" /></div>
                  </li>
                  <li>Propietarios (Email y total de alojamientos) que tienen 2 o más alojamientos activos.
                    <div class="col-md-12 text-center"><img src="dist/img/sql5.png" /></div>
                  </li>
                </ol>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="card card-primary">
                    <div class="card-header">
                      <h3 class="card-title">SQL's</h3>
                    </div>
                    <div class="card-body">
                      <!-- Añadir aquí las consultas SQL -->
                      <ol>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                      </ol>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </section>

        </div>

        <?php include_once 'docs/includes/footer.php'; ?>
      </div>
    </body>

</html>
