-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-12-2016 a las 08:51:50
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `test_avaibook`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alojamientos`
--

CREATE TABLE IF NOT EXISTS `alojamientos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_propietario` int(11) NOT NULL COMMENT 'Código del propietario al que pertenece',
  `nombre` varchar(250) DEFAULT NULL COMMENT 'Nombre del alojamiento',
  `tipo_alojamiento` enum('APARTAMENTO','PISO','CASA','HOTEL','CAMPING') NOT NULL,
  `estado` enum('ACTIVO','BAJA') NOT NULL DEFAULT 'ACTIVO',
  `anticipo` double(11,2) NOT NULL DEFAULT '25.00' COMMENT 'Importe a cobrar al realizar una reserva',
  PRIMARY KEY (`id`),
  KEY `cod_propietario` (`cod_propietario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `alojamientos`
--

INSERT INTO `alojamientos` (`id`, `cod_propietario`, `nombre`, `tipo_alojamiento`, `estado`, `anticipo`) VALUES
(1, 1, 'Hotel', 'HOTEL', 'ACTIVO', 25.00),
(2, 1, 'Hotel deLuxe', 'HOTEL', 'BAJA', 25.00),
(3, 1, 'Apartamentos en Salou', 'APARTAMENTO', 'ACTIVO', 50.00),
(4, 1, 'Casa rural Soria', 'CASA', 'ACTIVO', 75.00),
(5, 1, 'Loft en Barcelona', 'PISO', 'ACTIVO', 5.00),
(6, 1, 'Loft en Madrid', 'PISO', 'ACTIVO', 80.00),
(7, 2, 'Camping las palmas', 'CAMPING', 'ACTIVO', 50.00),
(8, 2, 'Camping montaña', 'CAMPING', 'ACTIVO', 25.00),
(9, 3, 'Hotel propi 3', 'HOTEL', 'BAJA', 25.00),
(10, 3, 'Camping propi 3', 'CAMPING', 'BAJA', 25.00),
(11, 4, 'Bonito apartamento', 'APARTAMENTO', 'ACTIVO', 34.00),
(12, 4, 'Bonito piso', 'PISO', 'BAJA', 33.00),
(13, 4, 'Bonita casa', 'CASA', 'BAJA', 15.00),
(14, 4, 'Bonito hotel', 'HOTEL', 'BAJA', 5.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitaciones`
--

CREATE TABLE IF NOT EXISTS `habitaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_alojamiento` int(11) NOT NULL COMMENT 'Código del alojamiento al que pertenece la habitación',
  `nombre` varchar(250) NOT NULL,
  `descripcion` text CHARACTER SET utf16,
  PRIMARY KEY (`id`),
  KEY `cod_alojamiento` (`cod_alojamiento`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Volcado de datos para la tabla `habitaciones`
--

INSERT INTO `habitaciones` (`id`, `cod_alojamiento`, `nombre`, `descripcion`) VALUES
(1, 1, 'Habitación doble ', 'Habitación 2 PAX'),
(2, 1, 'Habitación triple', 'Habitación 3 PAX'),
(3, 1, 'Habitación Suite Deluxe', 'La mas lujosa de todas las habitaciones del hotel'),
(4, 1, 'Habitación Suite', 'Suite con Jacuzzi'),
(5, 2, 'Suite luxuri', 'Jacuzzi y regalo de bienvenida\r\n'),
(6, 2, 'Suite nupcial', 'Ideal para recién casados'),
(7, 3, 'Apartamento 1', 'vistas al mar'),
(8, 3, 'Apartamento 2', 'segunda linea de playa'),
(9, 3, 'Apartamento 3', 'vistas al mar'),
(10, 3, 'Apartamento 4', 'segunda linea de playa'),
(11, 3, 'Apartamento 5', 'vistas al mar'),
(12, 3, 'Apartamento 6', 'vistas al mar'),
(13, 3, 'Apartamento 7', 'segunda linea de playa'),
(14, 4, 'Habitación de 3', 'cocina compartida'),
(15, 4, 'Habitación de 6 ', 'con literas'),
(16, 5, 'Loft', 'Cocina, salón y habitación en un mismo espacio!'),
(17, 6, 'Loft', 'Cocina, salón y habitación en un mismo espacio! En el centro de Madrid'),
(18, 7, 'Bungalow 4 pax', 'con todo lo necesario'),
(19, 7, 'Bungalow 6 pax', 'con todo lo necesario'),
(20, 7, 'Parcela caravana', 'para caravanas gigantes'),
(21, 7, 'Parcela quechua', 'para tiendas pequeñas'),
(22, 8, 'Parcela quechua', 'para tiendas pequeñas'),
(23, 8, 'Bungalow 4 pax', 'con todo lo necesario'),
(24, 8, 'Bungalow 6 pax', 'con todo lo necesario'),
(25, 8, 'Parcela caravana', 'para caravanas gigantes'),
(26, 9, 'Suite nupcial', 'Ideal para recién casados'),
(27, 9, 'Suite luxuri', 'Jacuzzi y regalo de bienvenida\r\n'),
(28, 10, 'Bungalow 6 pax', 'con todo lo necesario'),
(29, 10, 'Parcela caravana', 'para caravanas gigantes'),
(30, 11, 'Apartamento alquiler completo', 'vistas al mar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `propietarios`
--

CREATE TABLE IF NOT EXISTS `propietarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL COMMENT 'Email del registro',
  `nombre` varchar(250) DEFAULT NULL COMMENT 'Nombre del propietario',
  `f_alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de alta en el sistema',
  `estado` enum('ACTIVO','BAJA') NOT NULL DEFAULT 'ACTIVO' COMMENT 'Estado del propietario',
  `comision` double(11,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `propietarios`
--

INSERT INTO `propietarios` (`id`, `email`, `nombre`, `f_alta`, `estado`, `comision`) VALUES
(1, 'test1@email.com', 'Test número 1', '2020-10-27 22:39:43', 'ACTIVO', 15.30),
(2, 'test2@email.com', 'Test número 2', '2020-11-28 11:30:43', 'ACTIVO', 12.66),
(3, 'test3@email.com', 'Test número 3', '2020-09-28 09:39:43', 'BAJA', 30.50),
(4, 'test4@email.com', 'Test número 4', '2020-09-28 10:39:43', 'ACTIVO', 50.00),
(5, 'test5@email.com', 'Test número 5', '2020-11-28 09:39:43', 'ACTIVO', 40.70),
(6, 'test6@email.com', 'Test número 6', '2020-11-28 08:39:43', 'ACTIVO', 0.00);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alojamientos`
--
ALTER TABLE `alojamientos`
  ADD CONSTRAINT `alojamientos_ibfk_1` FOREIGN KEY (`cod_propietario`) REFERENCES `propietarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `habitaciones`
--
ALTER TABLE `habitaciones`
  ADD CONSTRAINT `habitaciones_ibfk_1` FOREIGN KEY (`cod_alojamiento`) REFERENCES `alojamientos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
