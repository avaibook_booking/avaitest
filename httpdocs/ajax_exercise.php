<?php
  /**************************************************************************************************/
  /*                                                                                                */
  /*                          FICHERO DE AJAX                                    */
  /*                                                                                                */
  /**************************************************************************************************/

  $path_raiz = './';

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $tituloPagina = 'AJAX';
    $menuSel = 'ajax';

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

?>


<!DOCTYPE html>
<html>

    <?php include_once 'docs/includes/head.php'; ?>

    <body class="hold-transition sidebar-mini layout-fixed">
      <div class="wrapper">

        <?php include_once 'docs/includes/sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

          <?php include_once 'docs/includes/header.php'; ?>

          <!-- /.content-header -->
           <!-- Main content -->
          <section class="content">
            <div class="container-fluid">

              <div class="callout callout-info">
                <h5><i class="fas fa-info"></i> AJAX</h5>
                <ol>
                  <li>Al hacer clic sobre el "Cargar info" de ambos bloques de Propietarios y Alojamientos, recuperar mediante AJAX el numero de propietarios y de alojamientos de base de datos y presentarlo en lugar del interrogante del bloque correspondiente.</li>
                  <li>Al hacer clic sobre el "Obtener info" del bloque de perfil, obtener la información sobre el propietario con ID del atributo <code>data-propietario-id</code> en base de datos y completar la información en la ficha de perfil.</li>
                </ol>
              </div>

              <!-- Small boxes (Stat box) -->
              <div class="row">
                <div class="col-lg-4 col-6 offset-lg-1">
                  <!-- small box -->
                  <div class="small-box bg-warning">
                    <div class="inner">
                      <h3>?</h3>

                      <p>Propietarios</p>
                    </div>
                    <div class="icon">
                      <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="small-box-footer">Cargar info <i class="fas fa-download ml-1"></i></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-4 col-6 offset-lg-2">
                  <!-- small box -->
                  <div class="small-box bg-info">
                    <div class="inner">
                      <h3>?</h3>

                      <p>Alojamientos</p>
                    </div>
                    <div class="icon">
                      <i class="ion ion-ios-home"></i>
                    </div>
                    <a href="#" class="small-box-footer">Cargar info <i class="fas fa-download ml-1"></i></a>
                  </div>
                </div>
                <!-- ./col -->
                
              </div>
              <div class="row">
                <!-- Profile Image -->
                <div class="card card-primary card-outline col-lg-4 col-6 offset-lg-1">
                  <div class="card-body box-profile">
                    <div class="text-center">
                      <img class="profile-user-img img-fluid img-circle"
                          src="../../dist/img/avatar.png"
                          alt="User profile picture">
                    </div>

                    <h3 class="profile-username text-center">¿Nombre?</h3>

                    <p class="text-muted text-center">¿Email?</p>

                    <ul class="list-group list-group-unbordered mb-3">
                      <li class="list-group-item">
                        <b>Alojamientos</b> <a class="float-right">?</a>
                      </li>
                      <li class="list-group-item">
                        <b>Habitaciones</b> <a class="float-right">?</a>
                      </li>
                    </ul>

                    <a href="#" data-propietario-id="1" class="btn btn-primary btn-block"><b>Obtener info</b></a>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
              </div>
            </div>
          </section>
        </div>
        <?php include_once 'docs/includes/footer.php'; ?>
      </div>
    </body>

</html>
