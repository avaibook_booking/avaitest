<?php
  /**************************************************************************************************/
  /*                                                                                                */
  /*                          FICHERO DE LISTADO DE PROPIETARIOS                                    */
  /*                                                                                                */
  /**************************************************************************************************/

    $path_raiz = './';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  $tituloPagina = 'AvaiTest';
  $menuSel = '';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

?>


<!DOCTYPE html>
<html>

    <?php include_once 'docs/includes/head.php'; ?>

    <body class="hold-transition sidebar-mini layout-fixed">
      <div class="wrapper">

        <?php include_once 'docs/includes/sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

          <?php include_once 'docs/includes/header.php'; ?>

          <!-- Main content -->
          <section class="content">

            <div class="container-fluid">

              <div class="callout callout-info">
                <h5><i class="fas fa-info"></i> AvaiTest</h5>
                Aquí tienes la explicación de los 3 ejercicios a realizar, los ficheros sobre los que trabajar están en la carpeta raíz.
                El ejercicio se compone de 3 partes
                <ul>
                  <li><strong>SQL</strong> En sql.php tienes las instrucciones para configurarte la BBDD, con 3 tablas muy sencillas
                    <ul>
                      <li>Propietarios - pueden tener de 0 a N alojamientos</li>
                      <li>Alojamientos - pueden tener de 1 a N habitaciones</li>
                      <li>Habitaciones</li>
                    </ul>
                  </li>
                  <li><strong>PHP</strong> En php.php tienes las instrucciones para realizar el apartado referente a PHP.</li>
                  <li><strong>AJAX</strong> En ajax_exercise.php tienes la parte de AJAX de la prueba.</li>
                </ul>

                En cualquiera de los ejercicios, puedes poner tu sello personal o usar la tecnologia que prefieras siempre y cuando cumplan los requisitos mínimos requeridos.<br>
                Una vez que lo tengas terminado, envíanoslo de nuevo en un fichero zip, en tu propio repositorio o como prefieras, el repo del que lo has clonado no permite escritura.<br>
                Cuéntanos como la has visto, fácil, difícil, media…En general el feeling que te ha dado….
              </div>
            </div>

          </section>

        </div>

        <?php include_once 'docs/includes/footer.php'; ?>
      </div>
    </body>

</html>
