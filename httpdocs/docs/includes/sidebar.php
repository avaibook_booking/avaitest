
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="dist/img/avaitest/imagotipo.png" alt="AvaiTest Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AvaiTest</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/avatar04.png" class="img-circle elevation-2" alt="AvaiUser Image">
        </div>
        <div class="info">
          <a href="index.php" class="d-block">AvaiUser Test</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="sql.php" class="nav-link <?php echo ($menuSel == 'sql') ? 'nav-link active' : ''; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ejercicio 1 - SQL</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="php.php" class="nav-link <?php echo ($menuSel == 'php') ? 'nav-link active' : ''; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ejecicio 2 - PHP</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="ajax_exercise.php" class="nav-link <?php echo ($menuSel == 'ajax') ? 'nav-link active' : ''; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ejecicio 3 - AJAX</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
