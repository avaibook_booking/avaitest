<?php
  /**************************************************************************************************/
  /*                                                                                                */
  /*                          FICHERO DE LISTADO DE PROPIETARIOS                                    */
  /*                                                                                                */
  /**************************************************************************************************/

    $path_raiz = './';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  $tituloPagina = 'Listado de propietarios';
  $menuSel = 'php';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

?>


<!DOCTYPE html>
<html>

    <?php include_once 'docs/includes/head.php'; ?>

    <body class="hold-transition sidebar-mini layout-fixed">
      <div class="wrapper">

        <?php include_once 'docs/includes/sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

          <?php include_once 'docs/includes/header.php'; ?>

          <!-- Main content -->
          <section class="content">

            <div class="container-fluid">

              <div class="callout callout-info">
                <h5><i class="fas fa-info"></i> PHP</h5>
                <ol>
                  <li>Recupera todos los propietarios y crea una tabla con los resultados, el listado debe mostrar id (propietario), email (que sea un enlace directo a enviar un email), nombre, fecha de alta (mostrada en formato 22/10/2016), estado y comisión.</li>
                  <li>Crea un filtro encima de la tabla que permita buscar por email (tiene que buscar por parte del email, no por coincidencia exacta) y otro por estado (desplegable).</li>
                </ol>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="card card-primary">
                    <div class="card-header">
                      <h3 class="card-title">Filtros de búsqueda</h3>
                    </div>
                    <div class="card-body">
                      <!-- Añadir aquí los filtros de búsqueda -->
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="card card-primary">
                    <div class="card-header">
                      <h3 class="card-title">Resultados</h3>
                    </div>
                    <div class="card-body">
                      <!-- Añadir aquí los resultados de la búsqueda -->
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </section>

        </div>

        <?php include_once 'docs/includes/footer.php'; ?>
      </div>
    </body>

</html>
